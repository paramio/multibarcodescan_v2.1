package com.google.zxing.client.android.multiple;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SettingGroupAdapter extends BaseAdapter{
	private Context context;
	 
    private List<String> list;
    public SettingGroupAdapter(Context context, List<String> list) {
    	 
        this.context = context;
        this.list = list;
 
    }
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		 	ViewHolder holder;
		 	//建立一个ViewHolder的缓存类，当convertview不为空时可以 直接使用缓存中的view 提高效率
	        if (convertView==null) {
	            convertView=LayoutInflater.from(context).inflate(R.layout.group_item_view, null);
	            holder=new ViewHolder();
	             
	            convertView.setTag(holder);
	             
	            holder.groupItem=(TextView) convertView.findViewById(R.id.groupItem);
	             
	        }
	        else{
	            holder=(ViewHolder) convertView.getTag();
	        }
	        holder.groupItem.setTextColor(Color.BLACK);
	        holder.groupItem.setText(list.get(position));
	         
	        return convertView;
	}

}
