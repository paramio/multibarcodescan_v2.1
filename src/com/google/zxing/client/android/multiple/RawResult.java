package com.google.zxing.client.android.multiple;

public class RawResult {
	private String format;
	private String content;
	
	
	public RawResult(String format,String content){
		this.format = format;
		this.content = content;
	}
	public String getFormat(){
		return this.format;
	}
	public String getContent(){
		return this.content;
	}
}
