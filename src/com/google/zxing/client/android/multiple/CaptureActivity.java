/*
 * Copyright (C) 2008 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.zxing.client.android.multiple;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.client.android.gallery.ShowImagesActivity;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.camera.AutoFocusManager;
import com.google.zxing.client.android.camera.CameraManager;
import com.google.zxing.client.android.multiple.R;
import com.google.zxing.client.android.result.ResultHandler;
import com.google.zxing.client.android.result.ResultHandlerFactory;

/**
 * This activity opens the camera and does the actual scanning on a background
 * thread. It draws a viewfinder to help the user place the barcode correctly,
 * shows feedback as the image processing is happening, and then overlays the
 * results when a scan is successful.
 * 
 * 此Activity所做的事：
 * 1. 开启camera，在后台独立线程中完成扫描任务；
 * 2. 绘制了一个扫描区（viewfinder）来帮助用户将条码置于其中以准确扫描；
 * 3. 扫描成功后会将扫描结果展示在界面上。
 * 
 * @author Sean Owen
 */
public final class CaptureActivity extends Activity implements
		Camera.AutoFocusCallback, SurfaceHolder.Callback {

	private static final String TAG = CaptureActivity.class.getSimpleName();

	public static final int HISTORY_REQUEST_CODE = 0x0000bacc;

	private CameraManager cameraManager;

	private CaptureActivityHandler handler;

	private Result savedResultToShow;

	private ViewfinderView viewfinderView;
	 /**
     * 扫描提示，例如"请将条码置于取景框内扫描"之类的提示
     */
	private TextView statusView;

	/**
     * 扫描结果展示窗口
     */
	private View resultView;

	private boolean hasSurface;


    /**
     * 【辅助解码的参数(用作MultiFormatReader的参数)】
     * 编码类型，该参数告诉扫描器采用何种编码方式解码，即EAN-13，QR Code等等
     * 对应于DecodeHintType.POSSIBLE_FORMATS类型
     * 参考DecodeThread构造函数中如下代码：hints.put(DecodeHintType.POSSIBLE_FORMATS, decodeFormats);
     */
	private Collection<BarcodeFormat> decodeFormats;

    /**
     * 【辅助解码的参数(用作MultiFormatReader的参数)】
     * 字符集，告诉扫描器该以何种字符集进行解码
     * 对应于DecodeHintType.CHARACTER_SET类型
     * 参考DecodeThread构造器如下代码：hints.put(DecodeHintType.CHARACTER_SET, characterSet);
     */
	private String characterSet;

    /**
     * 【辅助解码的参数(用作MultiFormatReader的参数)】
     * 该参数最终会传入MultiFormatReader，上面的decodeFormats和characterSet最终会先加入到decodeHints中
     * 最终被设置到MultiFormatReader中
     * 参考DecodeHandler构造器中如下代码：multiFormatReader.setHints(hints);
     */
	private Map<DecodeHintType, ?> decodeHints;


    /**
     * 活动监控器。如果手机没有连接电源线，那么当相机开启后如果一直处于不被使用状态则该服务会将当前activity关闭。
     * 活动监控器全程监控扫描活跃状态，与CaptureActivity生命周期相同。每一次扫描过后都会重置该监控，即重新倒计时。
     */
	private InactivityTimer inactivityTimer;

	 /**
     * 声音震动管理器。 扫描成功后可以播放提示音并震动，这两种功能都是用户自定义的。
     * 在Barcode Scanner中点击菜单键，点设置即可看到这两项的设置
     */
	private BeepManager beepManager;

	/**
     * 获取条形码显示结果内容布局，用于添加 和 清空
     */
	private LinearLayout content_layout;
	/**
     * 定时关闭结果处理机制
     */
	private Handler result_handler;

	protected boolean isFirst;
	/**
     * sd卡根目录变量
     */	
	private String SDcardPath;
	/**
	 * 存储rawresult
	 *
	 */
	private List<RawResult> resultsList;

	/**
	 * list项数量
	 * @return
	 */
	private int RESULTS_NUM = 5;
	/**
	 * SN/IMEI 格式
	 */
	private String FILE_NAME;
	/**
	 * SN格式
	 */
	private final String SN = "SN";
	/**
	 * IMEI格式
	 */
	private final String IMEI = "IMEI";
	/**
	 * setting 设置
	 * 
	 * @return
	 */
	private Button settingBunton;
	/**
	 * history 设置
	 * 
	 * @return
	 */
	private Button historyBunton;
	/**
	 * 控件PopupWindow 
	 */
	private PopupWindow popupWindow;
	/**
	 * 弹出框的ListView
	 */
	private ListView lv_group;
	/**
	 * 弹出选项 
	 */
	private View popupView;
	/**
	 * 列表
	 */
	private List<String> listGroups;

	/**
	 * 对应的弹出 这个对话框的指令
	 */
	private final int SINGLE_DIALOG = 0x113;
	/**
	 * 确认对话框
	 */
	protected String dialog_confirm;

	/**
	 * 获取 共享对象
	 */
	private SharedPreferences share;
	/**
	 * Preferences 编辑器
	 */
	private Editor editor;
	/**
	 * preferences 中存放的 格式 项
	 */
	protected final String FILE_FORMAT = "file_format";
	/**
	 * 读取历史记录的最大条数
	 */
	private final int MAX_RECORD_NUM = 4;
	/**
	 * 扫描界面的返回 按钮
	 */
	private Button scanbackButton;
	
	/**
	 * MultiBarScanner 文件路径
	 */
	public static String _filePath;
	/**
	 * index.txt路径
	 */
	public String txtPath;
	
	/************ variable and function boundary ************/

	ViewfinderView getViewfinderView() {
		return viewfinderView;
	}

	public Handler getHandler() {
		return handler;
	}

	CameraManager getCameraManager() {
		return cameraManager;
	}

	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);

		// 鍦ㄦ壂鎻忓姛鑳藉紑鍚悗锛屼繚鎸佸睆骞曞浜庣偣浜姸鎬�?
		Window window = getWindow();
		window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		setContentView(R.layout.capture);

		// 杩欓噷浠呬粎鏄鍚勪釜缁勪欢杩涜绠�鍗曠殑鍒涘缓鍔ㄤ綔锛�?湡姝ｇ殑鍒濆鍖栧姩浣滄斁鍦╫nResume涓�
		hasSurface = false;
		// Sd卡 根目录
		SDcardPath = Environment.getExternalStorageDirectory().getPath();
		// 拼接路径
		_filePath = SDcardPath + "/MultiBarScanner/";
		txtPath = _filePath + "index.txt";
		
		resultsList = new ArrayList<RawResult>();
		inactivityTimer = new InactivityTimer(this);
		beepManager = new BeepManager(this);
		// myPreferences = new SharePreferences();
		// 鍔犺浇preferences.xml 涓汉閰嶇疆鏂囦�?
		SharePreferences();
		FILE_NAME = getParameterString(FILE_FORMAT, IMEI);

	}

	@SuppressLint("HandlerLeak")
	@Override
	protected void onResume() {
		super.onResume();
		isFirst = false;
		// 鍒濆鍖杛esultview 娓呴櫎textview
		removeResultsTextView();
		// CameraManager must be initialized here, not in onCreate(). This is
		// necessary because we don't
		// want to open the camera driver and measure the screen size if we're
		// going to show the help on
		// first launch. That led to bugs where the scanning rectangle was the
		// wrong size and partially
		// off screen.
		/**
		 * 鐩告�?鍒濆鍖栫殑鍔ㄤ綔闇�瑕佸紑鍚浉鏈哄苟娴嬮噺灞忓箷澶у皬锛岃繖浜涙搷浣�
		 * 涓嶅缓璁斁鍒皁nCreate涓紝鍥犱负濡傛灉鍦╫nCreate涓姞涓婇娆�?�惎鍔ㄥ睍绀哄府鍔╀俊鎭殑浠ｇ爜鐨�?
		 * 璇濓紝浼氬鑷存壂鎻忕獥鍙ｇ殑灏哄璁＄畻鏈夎鐨刡ug
		 */
		cameraManager = new CameraManager(getApplication());

		viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
		viewfinderView.setCameraManager(cameraManager);
		resultView = findViewById(R.id.result_view);
		statusView = (TextView) findViewById(R.id.status_view);
		scanbackButton = (Button)findViewById(R.id.scanback_button);
		settingBunton = (Button) findViewById(R.id.setting_button);
		historyBunton = (Button) findViewById(R.id.history_button);

		handler = null;

		/*
		 * 閲嶇疆鐘舵�佺獥鍙ｏ紝鎵弿绐楀彛鍜�?粨鏋滅獥鍙ｇ殑鐘舵��? 淇敼涔嬪墠鐨� //resetStatusView(); *
		 */
		// 鍒濆鍖栧悇涓獀iew 骞剁�? resultView娣诲姞touch浜嬩�?
		initViewEvent();
		// 加载setting设置控件
		loadSettingButtonListener();
		//加载最近历史记录
		loadHistoryButtonListener();
		//加载 扫描界面 返回
		loadScanbackListener();
		result_handler = new Handler() {
			@Override
			public void handleMessage(Message msg) { // handle message
				switch (msg.what) {
				case 1:
					restartPreviewAfterDelay(1000);
					findViewById(R.id.result_view_sub).setVisibility(
							View.VISIBLE);
					findViewById(R.id.result_view_textclick).setVisibility(
							View.GONE);
				}

				super.handleMessage(msg);
			}
		};
		// 鎽勫儚澶撮瑙堝姛鑳藉繀椤诲�熷姪SurfaceView锛屽洜姝や篃闇�瑕佸湪涓�寮�濮嬪鍏惰繘琛屽垵濮嬪�?
		SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
		SurfaceHolder surfaceHolder = surfaceView.getHolder();
		if (hasSurface) {
			// The activity was paused but not stopped, so the surface still
			// exists. Therefore
			// surfaceCreated() won't be called, so init the camera here.
			initCamera(surfaceHolder);
		} else {
			// Install the callback and wait for surfaceCreated() to init the
			// camera.
			// 濡傛灉SurfaceView宸茬粡娓叉煋瀹屾瘯锛屼細鍥炶皟surfaceCreated锛屽湪surfaceCreated涓皟鐢╥nitCamera()
			surfaceHolder.addCallback(this);
		}

		// 鍔犺浇澹伴煶閰嶇疆锛屽叾瀹炲湪BeemManager鐨勬瀯閫犲櫒涓篃浼氳皟鐀��鏂规硶锛屽嵆鍦╫nCreate鐨勬椂鍊欎細璋冪敤涓�娆�?
		beepManager.updatePrefs();

		// 鎭㈠娲诲姩鐩戞帶鍣�?
		inactivityTimer.onResume();

	}
	/**
	 * 加载 扫描界面的返回 事件
	 */
	private void loadScanbackListener(){
		scanbackButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				isFirst = false;
				// 鍒濆鍖杛esultview 娓呴櫎textview
				removeResultsTextView();
				// 鍒濆鍖栧悇涓獀iew 骞剁�? resultView娣诲姞touch浜嬩�?
				initViewEvent();
			}
		});
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		removeResultsTextView();
	}

	@Override
	protected void onPause() {
		isFirst = false;
		if (handler != null) {
			handler.quitSynchronously();
			handler = null;
		}

		// 鏆傚仠娲诲姩鐩戞帶鍣�?
		inactivityTimer.onPause();

		// 鍏抽棴鎽勫儚澶�
		cameraManager.closeDriver();
		if (!hasSurface) {
			SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
			SurfaceHolder surfaceHolder = surfaceView.getHolder();
			surfaceHolder.removeCallback(this);
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		// 鍋滄娲诲姩鐩戞帶鍣�?
		inactivityTimer.shutdown();
		super.onDestroy();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		// case KeyEvent.KEYCODE_BACK: // 鎷︽埅杩斿洖閿�
		// removeResultsTextView();
		// restartPreviewAfterDelay(0L);
		// return true;

		case KeyEvent.KEYCODE_FOCUS:
		case KeyEvent.KEYCODE_CAMERA:
			// Handle these events so they don't launch the Camera app
			return true;
			// Use volume up/down to turn on light
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			cameraManager.setTorch(false);
			return true;
		case KeyEvent.KEYCODE_VOLUME_UP:
			cameraManager.setTorch(true);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.close();

		return false;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

	}

	/**
	 * 鍚慍aptureActivityHandler涓彂閫佹秷鎭紝骞跺睍�?烘壂鎻忓埌鐨勫浘鍍�
	 * 
	 * @param bitmap
	 * @param result
	 */
	private void decodeOrStoreSavedBitmap(Bitmap bitmap, Result result) {
		// Bitmap isn't used yet -- will be used soon
		if (handler == null) {
			savedResultToShow = result;
		} else {
			if (result != null) {
				savedResultToShow = result;
			}
			if (savedResultToShow != null) {
				Message message = Message.obtain(handler,
						R.id.decode_succeeded, savedResultToShow);
				handler.sendMessage(message);
			}
			savedResultToShow = null;
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if (holder == null) {
			Log.e(TAG,
					"*** WARNING *** surfaceCreated() gave us a null surface!");
		}
		if (!hasSurface) {
			hasSurface = true;
			initCamera(holder);
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		hasSurface = false;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}

	/**
	 * A valid barcode has been found, so give an indication of success and show
	 * the results.
	 * 
	 * @param scaleFactor
	 *            amount by which thumbnail was scaled
	 * @param barcode
	 *            A greyscale bitmap of the camera data which was decoded.
	 */
	public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
		inactivityTimer.onActivity();
		// 瑙ｆ瀽rawResult锛屾牴鎹笉鍚岀被鍨媟esult鐢熸垚�?�瑰簲鐨凴esultHandler
		ResultHandler resultHandler = ResultHandlerFactory.makeResultHandler(
				this, rawResult);

		boolean fromLiveScan = barcode != null;
		if (fromLiveScan) {
			// Then not from history, so beep/vibrate and we have an image to
			// draw on
			// beepManager.playBeepSoundAndVibrate(); //鎵弿鎴愬姛楦ｅ�?
			// drawResultPoints(barcode, scaleFactor, rawResult);
		}

		handleDecodeInternally(rawResult, resultHandler, barcode);

	}

	/**
	 * Superimpose a line for 1D or dots for 2D to highlight the key features of
	 * the barcode.
	 * 
	 * @param barcode
	 *            A bitmap of the captured image.
	 * @param scaleFactor
	 *            amount by which thumbnail was scaled
	 * @param rawResult
	 *            The decoded results which contains the points to draw.
	 */
	private void drawResultPoints(Bitmap barcode, float scaleFactor,
			Result rawResult) {
		ResultPoint[] points = rawResult.getResultPoints();
		if (points != null && points.length > 0) {
			Canvas canvas = new Canvas(barcode);
			Paint paint = new Paint();
			paint.setColor(getResources().getColor(R.color.result_points));
			if (points.length == 2) {
				paint.setStrokeWidth(4.0f);
				drawLine(canvas, paint, points[0], points[1], scaleFactor);
			} else if (points.length == 4
					&& (rawResult.getBarcodeFormat() == BarcodeFormat.UPC_A || rawResult
							.getBarcodeFormat() == BarcodeFormat.EAN_13)) {
				// Hacky special case -- draw two lines, for the barcode and
				// metadata
				drawLine(canvas, paint, points[0], points[1], scaleFactor);
				drawLine(canvas, paint, points[2], points[3], scaleFactor);
			} else {
				paint.setStrokeWidth(10.0f);
				for (ResultPoint point : points) {
					if (point != null) {
						canvas.drawPoint(scaleFactor * point.getX(),
								scaleFactor * point.getY(), paint);
					}
				}
			}
		}
	}

	private static void drawLine(Canvas canvas, Paint paint, ResultPoint a,
			ResultPoint b, float scaleFactor) {
		if (a != null && b != null) {
			canvas.drawLine(scaleFactor * a.getX(), scaleFactor * a.getY(),
					scaleFactor * b.getX(), scaleFactor * b.getY(), paint);
		}
	}

	// Put up our own UI for how to handle the decoded contents.
	/**
	 * 璇ユ柟娉曚細灏嗘渶缁堝鐞嗙殑缁撴灉灞曠ず鍒皉esult_view涓娿�傚苟涓斿鏋滈�夋嫨浜�?"妫�绱㈡洿澶氫俊鎭�"
	 * 鍒欎細鍐呴儴瀵圭粨鏋滆繘琛岃繘涓�姝ョ殑鏌ヨ�?
	 * 
	 * @param rawResult
	 * @param resultHandler
	 * @param barcode
	 */
	private void handleDecodeInternally(Result rawResult,
			ResultHandler resultHandler, Bitmap barcode) {

		// 妫�绱㈠悇涓潯鐮�
		String SN = null;
		String IMEI = null;
		String ICCID = null;

		String fileName = null;
		String format = rawResult.getBarcodeFormat().toString();
		String content = resultHandler.getDisplayContents().toString();

		Log.d("_results", format + ":" + content);

		// 灏唕awResult鐨勭被鍨嬪拰鍐呭�? 寰幆娣诲姞杩涘垪琛�?
		resultsList.add(new RawResult(format, content));
		// 限制扫描的结果数量 
		if (resultsList.size() >= RESULTS_NUM) {
			//隐藏 几个 显示控件
			scanbackButton.setVisibility(View.GONE);
			statusView.setVisibility(View.GONE);
			viewfinderView.setVisibility(View.GONE);
			//显示结果布局
			resultView.setVisibility(View.VISIBLE);

			ImageView barcodeImageView = (ImageView) findViewById(R.id.barcode_image_view);
			if (barcode == null) {
				barcodeImageView.setImageBitmap(BitmapFactory.decodeResource(
						getResources(), R.drawable.launcher_icon));
			} else {
				//向 imageView里放扫描的图片
				barcodeImageView.setImageBitmap(barcode);

				for (RawResult rawresult : resultsList) {
					if (rawresult.getContent().trim().charAt(0) == 'S') {
						SN = rawresult.getContent();
					} else if (rawresult.getContent().trim().length() == 20) {
						ICCID = rawresult.getContent();
					} else if (rawresult.getContent().trim().length() == 15) {
						IMEI = rawresult.getContent();
					}
				}
				if (FILE_NAME.equals("IMEI")) {
					fileName = IMEI;
				} else if (FILE_NAME.equals("SN")) {
					fileName = SN;
				}
				String savedFilePath = _filePath + fileName + ".jpg";
				try {
					saveBitmapToFile(barcode, savedFilePath);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				SimpleDateFormat dateformat = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:ss");
				Date date = new Date();
				String _date = dateformat.format(date);
				String newContents =  _date + ":" + FILE_NAME + " Model# \t"
						+ IMEI + "\t" + SN + "\t" + ICCID + "\n";				
				//读取文件index.txt 的内容 去除重复的 IMEI 并存入一个list
				List<String> contentsList = readToList(txtPath, fileName);				
				File file =new File(txtPath);
				//删除当前存在的文件 之后在创建
				if(file.exists()){
					file.delete();
				}
				for (String contents : contentsList) {
					newContents += contents;
				}	
				//写入 对应路径的内存卡中
				writeFileSdcard(txtPath,newContents);
				//发送 扫描更新gallery
				requestScanFile(CaptureActivity.this, new File(savedFilePath));
			}

			DateFormat formatter = DateFormat.getDateTimeInstance(
					DateFormat.SHORT, DateFormat.SHORT);
			TextView timeTextView = (TextView) findViewById(R.id.time_text_view);
			timeTextView.setText(formatter.format(new Date(rawResult
					.getTimestamp())));

			content_layout = (LinearLayout) findViewById(R.id.contents_Layout);
			for (RawResult _result : resultsList) {
				TextView contentsTextView = new TextView(
						content_layout.getContext());
				contentsTextView.setTextSize(20L);
				contentsTextView.setText(_result.getFormat() + ":"
						+ _result.getContent());
				content_layout.addView(contentsTextView);
				contentsTextView = null;
			}

			content_layout = null;
			resultsList.clear();
			// 计时3秒钟重新扫描
			Message message = result_handler.obtainMessage(1);
			// Message
			result_handler.sendMessageDelayed(message, 3000);
			Log.d("CpatureActivity", "send Message success!");
		}

	}
	/**
	 * Read each line to the list and remove the same item
	 */
	public  List<String> readToList(String filePath, String imageName){
		String encoding="GBK";
		int i = 0;
		List<Integer> indexList = new ArrayList<Integer>();
		List<String> indexContentsList = new ArrayList<String>();
		Log.d("clipContentsList","ok");
    	try {
            File file=new File(filePath);
            if(file.isFile() && file.exists()){ //判断文件是否存在
                InputStreamReader read = new InputStreamReader(
                new FileInputStream(file),encoding);//考虑到编码格式
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt = null;
                while((lineTxt = bufferedReader.readLine()) != null){  
                	//将读取的一行数据存储到列表中
                	indexContentsList.add(lineTxt);
                	Log.d("clipContentsList",lineTxt);
                    List<String> list = ShowImagesActivity.clipContents(lineTxt);                    
                    //判断所读的 一行中 是否有对应的 图片名存在 如果存在则取出这一行
                    for (String item : list) {
                    	Log.d("clipContentsList",item+"||"+imageName);
                    	if(item.trim().equals(imageName.trim())){ 
                    		//将重复的一条记录 存储在列表中
                    		indexList.add(i);
                    		
                    	}							
					}   
                    i++;
                } 
                i = 0;
                //关闭读取
                read.close();
                Log.d("indexListSize", String.valueOf(indexContentsList.size())+"||"+String.valueOf(indexList.size()));
                
                if(indexList.size() != 0){
	                //去除列表中重复的记录
	                for (int index : indexList) {
						indexContentsList.set(index, null);
					}
	               List<String> newList = new ArrayList<String>();
	               for (String item : indexContentsList) {
					if(item != null){
						newList.add(item);
					}
	               }
	              
	               indexContentsList = newList;
	               Log.d("indexListSize", String.valueOf(indexContentsList.size()));
                }
                indexList.clear();
		    }else{
		    	
//		    	ShowImagesActivity.setToast(CaptureActivity.this, 
//		        		"cannot find the file!", Toast.LENGTH_LONG);
		    }
    	}catch (Exception e) { 
            e.printStackTrace(); 
        } 
    	return indexContentsList;
	}
	/**
	 * 发送扫描广播 更新gallery
	 * @param context
	 * @param file
	 */
	public static void requestScanFile(Context context, File file) {
	    Intent i = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
	    i.setData(Uri.fromFile(file));
	    context.sendBroadcast(i);
	}
	@Override
	public void closeContextMenu() {
		// TODO Auto-generated method stub

		super.closeContextMenu();
	}

	@Override
	public void onContextMenuClosed(Menu menu) {
		// TODO Auto-generated method stub

		super.onContextMenuClosed(menu);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub

		super.onCreateContextMenu(menu, v, menuInfo);
	}

	// Briefly show the contents of the barcode, then handle the result outside
	// Barcode Scanner.

	private void sendReplyMessage(int id, Object arg, long delayMS) {
		if (handler != null) {
			Message message = Message.obtain(handler, id, arg);
			if (delayMS > 0L) {
				handler.sendMessageDelayed(message, delayMS);
			} else {
				handler.sendMessage(message);
			}
		}
	}

	private void initCamera(SurfaceHolder surfaceHolder) {
		if (surfaceHolder == null) {
			throw new IllegalStateException("No SurfaceHolder provided");
		}
		if (cameraManager.isOpen()) {
			Log.w(TAG,
					"initCamera() while already open -- late SurfaceView callback?");
			return;
		}
		try {
			cameraManager.openDriver(surfaceHolder);
			// Creating the handler starts the preview, which can also throw a
			// RuntimeException.
			if (handler == null) {
				handler = new CaptureActivityHandler(this, decodeFormats,
						decodeHints, characterSet, cameraManager);
			}
			decodeOrStoreSavedBitmap(null, null);
		} catch (IOException ioe) {
			Log.w(TAG, ioe);
			displayFrameworkBugMessageAndExit();
		} catch (RuntimeException e) {
			// Barcode Scanner has seen crashes in the wild of this variety:
			// java.?lang.?RuntimeException: Fail to connect to camera service
			Log.w(TAG, "Unexpected error initializing camera", e);
			displayFrameworkBugMessageAndExit();
		}
	}

	/**
	 * 娓呴�? resultview閲岀�? textView
	 */
	public void removeResultsTextView() {
		content_layout = (LinearLayout) findViewById(R.id.contents_Layout);
		// content_layout.removeAllViews();
		int count = content_layout.getChildCount();
		Log.d("chidren", String.valueOf(count));
		if (count != 0) {
			content_layout.removeViews(0, count);
		}
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		removeResultsTextView();
	}

	@Override
	protected void onRestart() {
		// TODO Auto-generated method stub
		super.onRestart();
		removeResultsTextView();
	}

	private void displayFrameworkBugMessageAndExit() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.app_name));
		builder.setMessage(getString(R.string.msg_camera_framework_bug));
		builder.setPositiveButton(R.string.button_ok, new FinishListener(this));
		builder.setOnCancelListener(new FinishListener(this));
		builder.show();
	}

	/**
	 * 鍦ㄧ粡杩囦竴娈靛欢杩熷悗閲嶇疆鐩告満浠ヨ繘琛屼笅涓�娆℃壂鎻忋��? 鎴愬姛鎵弿杩囧悗鍙皟鐀��鏂规硶绔嬪埢鍑嗗杩涜涓嬫鎵�?
	 * 
	 * @param delayMS
	 */
	public void restartPreviewAfterDelay(long delayMS) {
		if (handler != null) {
			handler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
		}

		resetStatusView();
	}

	/**
	 * 灞曠ず鐘舵�佽鍥惧拰鎵弿绐楀彛锛岄殣钘忕粨鏋滆鍥�
	 */
	private void resetStatusView() {
		scanbackButton.setVisibility(View.VISIBLE);
		resultView.setVisibility(View.GONE);
		statusView.setText(R.string.msg_default_status);
		statusView.setVisibility(View.VISIBLE);
		viewfinderView.setVisibility(View.VISIBLE);
		removeResultsTextView();
	}

	public void drawViewfinder() {
		viewfinderView.drawViewfinder();
	}

	/**
	 * 鍒濆鍖栭瑙堢晫闈�? 娣诲姞瑙︽懜浜嬩欢杩涘叆鎵弿鐘舵��?
	 */
	public void initViewEvent() {
		findViewById(R.id.result_view_sub).setVisibility(View.GONE);
		resultView.setVisibility(View.VISIBLE);
		statusView.setText("");
		scanbackButton.setVisibility(View.GONE);
		statusView.setVisibility(View.GONE);
		viewfinderView.setVisibility(View.GONE);
		findViewById(R.id.result_view_textclick).setVisibility(View.VISIBLE);
		// 闅愯�? result閲岀殑view
		Button scannerButton = (Button) findViewById(R.id.scanner_button);
		// findViewById(R.id.result_button_view).setVisibility(ViewGroup.INVISIBLE);
		scannerButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (!isFirst) {
					restartPreviewAfterDelay(1000);
					findViewById(R.id.result_view_sub).setVisibility(
							View.VISIBLE);
					// findViewById(R.id.result_button_view).setVisibility(ViewGroup.VISIBLE);
					findViewById(R.id.result_view_textclick).setVisibility(
							View.GONE);
					isFirst = true;
				}
			}
		});
		viewfinderView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				cameraManager.getCamera().autoFocus(CaptureActivity.this);
				return true;
			}
		});

		// ((ToggleButton)findViewById(R.id.toggle_save)).setOnCheckedChangeListener(new
		// OnCheckedChangeListener() {
		// //閫氳繃鐐瑰嚮togglebutton 鍒囨崲淇濆瓨鍥剧墖鐨勬ā寮�?
		// @Override
		// public void onCheckedChanged(CompoundButton buttonView, boolean
		// isChecked) {
		// // TODO Auto-generated method stub
		// if(isChecked){
		// FILE_NAME = "IMEI";
		// }else{
		// FILE_NAME = "SN";
		// }
		// }
		// });
	}

	@Override
	public void onAutoFocus(boolean arg0, Camera arg1) {
		// TODO Auto-generated method stub
		cameraManager.getCamera().cancelAutoFocus();
	}

	/**
	 * 鍐欙�? 璇籹dcard鐩綍涓婄殑鏂囦欢锛岃鐢�?�ileOutputStream锛� 涓嶈兘鐢╫penFileOutput
	 * 涓嶅悓鐐癸細openFileOutput鏄湪raw閲岀紪璇戣繃鐨勶紝FileOutputStream鏄换浣曟枃浠堕兘鍙互
	 * 
	 * @param fileName
	 * @param message
	 */
	// 写入/mnt/sdcard/index.txt
	public static void writeFileSdcard(String filePath, String message) {
		try {			
			// FileOutputStream(String name, boolean append)
			FileOutputStream fout = new FileOutputStream(filePath, true);
			byte[] bytes = message.getBytes();
			fout.write(bytes);
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save Bitmap to a file.淇濆瓨鍥剧墖鍒癝D鍗°��?
	 * 
	 * @param bitmap
	 * @param file
	 * @return error message if the saving is failed. null if the saving is
	 *         successful.
	 * @throws IOException
	 */
	public static void saveBitmapToFile(Bitmap bitmap, String _file)
			throws IOException {
		BufferedOutputStream os = null;
		try {
			File file = new File(_file);
			// String _filePath_file.replace(File.separatorChar +
			// file.getName(), "");
			int end = _file.lastIndexOf(File.separator);
			String _filePath = _file.substring(0, end);
			File filePath = new File(_filePath);
			if (!filePath.exists()) {
				filePath.mkdirs();
			}
			file.createNewFile();
			os = new BufferedOutputStream(new FileOutputStream(file));
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					Log.e("save images", e.getMessage(), e);
				}
			}
		}
	}
	/**
	 * 加载 历史记录
	 */
	private void loadHistoryButtonListener(){
		historyBunton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				showHistoryDialog();
			}
		});
	}
	/**
	 * 弹出 历史dialog对话框
	 */
	private void showHistoryDialog(){
		List<String> list = readRecentlyHistory(txtPath);
		String[] records = (String[])list.toArray(new String[list.size()]);			
		Builder history = new AlertDialog.Builder(CaptureActivity.this);
		history.setTitle("Recently Scanned:");
		history.setItems(records, null);
		history.show();
	}
	/**
	 * 读取最近的 4条记录
	 */
	private List<String> readRecentlyHistory(String filepath){
		int i = 0;
		List<String> records = new ArrayList<String>();
		String encoding = "GBK";
		try {
			File file = new File(filepath);								
			if(file.exists()){						
				InputStreamReader read = new InputStreamReader(
		                new FileInputStream(file),encoding );//考虑到编码格式
	            BufferedReader bufferedReader = new BufferedReader(read);
	            String lineTxt = null;	          
				while((lineTxt = bufferedReader.readLine()) != null){	
					Log.d("readRecentlyHistory", lineTxt);
					records.add(lineTxt);
					i++;					
					if(i >= MAX_RECORD_NUM){						
						break;
					}
				}
				i = 0;
				read.close();				  
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			ShowImagesActivity.setToast(CaptureActivity.this, 
					"Get recent record failed!", Toast.LENGTH_SHORT);
			e.printStackTrace();
		}            	            
		return records;		
	}
	/**
	 * 加载setting设置的方法
	 */
	private void loadSettingButtonListener() {
		settingBunton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showPopupWindow(v);
			}
		});
	}

	/**
	 * 弹出setting 设置列表
	 */
	private void showPopupWindow(View parent) {

		if (popupWindow == null) {
			LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			popupView = layoutInflater.inflate(R.layout.group_list, null);

			lv_group = (ListView) popupView.findViewById(R.id.lvGroup);
			// 鍔犺浇鏁版嵁
			listGroups = new ArrayList<String>();
			//SN/IMEI
//			listGroups.add(getResources().getString(R.string.list_item_format));
			listGroups.add(getResources().getString(R.string.list_item_email));
			listGroups.add(getResources().getString(R.string.list_item_exit));
			SettingGroupAdapter groupAdapter = new SettingGroupAdapter(this,
					listGroups);
			lv_group.setAdapter(groupAdapter);
			// 鍒涘缓涓�涓狿opuWidow瀵硅�?
			popupWindow = new PopupWindow(popupView, 300, 400);
			popupWindow.setHeight(LayoutParams.WRAP_CONTENT);
			// popupWindow.setWidth(LayoutParams.WRAP_CONTENT);

		}

		// 浣垮叾鑱氶泦
		popupWindow.setFocusable(true);
		// 璁剧疆鍏佽鍦ㄥ鐐瑰嚮娑堝�?
		popupWindow.setOutsideTouchable(true);

		// 杩欎釜鏄负浜嗙偣鍑烩�滆繑鍥濨ack鈥濅篃鑳戒娇鍏舵秷澶憋紝骞朵笖骞朵笉浼氬奖鍝嶄綘鐨勮儗鏅�?
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		WindowManager windowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		// 鏄剧ず鐨勪綅缃�
		popupWindow.showAsDropDown(parent, 0, 0);

		lv_group.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int position, long id) {

				// Toast.makeText(CaptureActivity.this,
				// listGroups.get(position), 1000)
				// .show();
				switch (position) {
//				case 0:
//					showDialog(SINGLE_DIALOG);
//					break;
				case 0:
					// start gallery
					Intent intent = new Intent(CaptureActivity.this,
							ShowImagesActivity.class);
					startActivity(intent);
					break;
				case 1:
					exitApp();// 閫�鍑虹▼搴�?
					break;
				}

				if (popupWindow != null) {
					popupWindow.dismiss();
				}
			}
		});
	}

	/**
	 * 杩欐槸鐢ㄦ潵鍒涘缓�?�硅瘽妗嗙殑鏂规硶锛�?户鎵胯嚜Activity涓嶈繃鍦ㄦ柊鐨勭増涓凡缁忚寮冪敤浜�
	 * 鍙�氳繃showDialog璋冪�?
	 */
	@Override
	@Deprecated
	protected Dialog onCreateDialog(int id, Bundle args) {
		// TODO Auto-generated method stub
		Log.d("dialog", "create dialog ok!");
		Dialog d = null;
		switch (id) {
		case SINGLE_DIALOG:
			d = singleDialog();
			break;
		}
		// 杩斿洖寮瑰嚭瀵硅瘽妗�?
		return d;
	}

	protected Dialog singleDialog() {
		int index = 0;
		String format = getParameterString(FILE_FORMAT, IMEI).trim();
		// 鍔犺浇preferences閰嶇疆涓殑榛樿閫夐�??
		if (format.equals(SN)) {
			index = 1;
		} else if (format.equals(IMEI)) {
			index = 0;
		} else {
		}
		// 瀵硅瘽妗嗘�?�閫犲櫒
		Builder b = new AlertDialog.Builder(this);
		b.setTitle(getResources().getString(R.string.single_dialog));
		b.setSingleChoiceItems(new String[] {"IMEI","SN"}, index,
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						switch (which) {
						// 閫夋�? 涓�涓牸寮�?
						case 1:
							FILE_NAME = SN;
							// 灏嗛�夋嫨鐨勬牸寮� 淇濆�? 涓嬫鍚姩鍚庡姞杞�?
							setParameterString(FILE_FORMAT, SN);
							break;
						case 0:

							FILE_NAME = IMEI;
							setParameterString(FILE_FORMAT, IMEI);
							Log.d("FILE_NAME", FILE_NAME);
							break;
						}

						dialog_confirm = getResources().getString(
								R.string.single_dialog_ok_button)
								+ FILE_NAME;
					}
				});
		// Log.d("FILE_NAME",FILE_NAME);
		// 娣诲姞涓�涓�?�璁ょ殑瀵硅瘽妗�?
		b.setPositiveButton(R.string.button_ok, new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// 纭娑堟伅
				Toast.makeText(CaptureActivity.this, dialog_confirm, 1000)
						.show();
			}
		});
		// 鍒涘缓�?�硅瘽妗�?
		return b.create();
	}

	/**
	 * 鍒涘缓涓�涓猄harePreferences 鐀��潵淇敼瀛樺偍鐢ㄦ埛閰嶇疆鐨勬枃浠�
	 */
	public void SharePreferences() {
		// 鑾峰彇鍏朵粬鍖呭唴鐨刾references
		share = CaptureActivity.this.getSharedPreferences("perferences",
				Context.MODE_APPEND);
		editor = share.edit();
	}

	/**
	 * 璁剧疆�?�楃涓插弬鏁�
	 * 
	 * @param key
	 * @param value
	 */
	public void setParameterString(String key, String value) {
		if (editor != null) {
			editor.putString(key, value);
			editor.commit();
		}
	}

	/**
	 * 鑾峰彇�?�楃涓插弬鏁�
	 * 
	 * @param key
	 * @param defValue
	 * @return
	 */
	public String getParameterString(String key, String defValue) {
		// 杩斿�? 鍊� 绗竴涓弬鏁颁�? 鎯宠鑾峰彇鐨� 閿�� 绗簩涓�? 鍙傛暟鏄�? 褰撴鏌ヤ笉鍒拌繖涓�兼椂
		// 鐨勯粯璁よ繑鍥炲��
		if (share != null) {
			return share.getString(key, defValue);
		}
		return null;
	}

	/**
	 * 璁剧疆鏁村�?�鍙傛暟
	 * 
	 * @param key
	 * @param value
	 */
	public void setParameterInt(String key, int value) {
		if (editor != null) {
			editor.putInt(key, value);
			editor.commit();
		}
	}

	/**
	 * 鑾峰彇鏁村�??
	 * 
	 * @param key
	 * @param defValue
	 * @return
	 */
	public int getParameterInt(String key, int defValue) {
		// 杩斿�? 鍊� 绗竴涓弬鏁颁�? 鎯宠鑾峰彇鐨� 閿�� 绗簩涓�? 鍙傛暟鏄�? 褰撴鏌ヤ笉鍒拌繖涓�兼椂
		// 鐨勯粯璁よ繑鍥炲��
		if (share != null) {
			return share.getInt(key, defValue);
		}
		return 0;
	}

	/**
	 * 鐀��簬閫�鍑虹▼搴� 瀵硅瘽妗�?
	 */
	private void exitApp() {

		AlertDialog alertDialog = new AlertDialog.Builder(this)
				.setTitle(
						getResources().getString(
								R.string.exit_dialog_notify_caution))
				.setMessage(
						getResources().getString(
								R.string.exit_dialog_notify_notify))
				.setPositiveButton(
						getResources()
								.getString(R.string.exit_dialog_notify_ok),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								// 杩斿洖涓荤晫闈�
								Intent intent = new Intent(Intent.ACTION_MAIN);
								intent.addCategory(Intent.CATEGORY_HOME);
								// 鎴戜滑鐭ラ亾Android鐨勭獥鍙ｇ被鎻愪緵浜嗗巻鍙叉爤锛屾垜浠彲浠ラ�氳繃stack鐨勫師鐞嗘潵宸у鐨勫疄鐜帮紝
								// 杩欓噷鎴戜滑鍦�?�绐�?彛鎵撳紑A绐楀彛鏃跺湪Intent涓洿鎺ュ姞鍏ユ爣蹇桰ntent.FLAG_ACTIVITY_CLEAR_TOP锛�
								// 鍐嶆寮�鍚疉鏃跺皢浼氭竻闄よ杩涚▼绌洪棿鐨勬墍鏈堿ctivity銆�
								intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
								startActivity(intent);
								// 缁撴潫鑷繁鐨勮繘绋�?
								android.os.Process
										.killProcess(android.os.Process.myPid());
							}

						})
				.setNegativeButton(
						getResources().getString(
								R.string.exit_dialog_notify_cancel),

						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								return;
							}
						}).create(); // 鍒涘缓�?�硅瘽妗�?
		alertDialog.show(); // 鏄剧ず�?�硅瘽妗�?
	}
}
