# view res/layout/grid_child_item.xml #generated:11
-keep class com.client.android.gallery.MyImageView { <init>(...); }

# view AndroidManifest.xml #generated:54
-keep class com.client.android.gallery.ShowImagesActivity { <init>(...); }

# view res/layout/grid_group_item.xml #generated:11
-keep class com.example.imagescan.MyImageView { <init>(...); }

# view AndroidManifest.xml #generated:41
-keep class com.google.zxing.client.android.multiple.CaptureActivity { <init>(...); }

# view res/layout/capture.xml #generated:9
-keep class com.google.zxing.client.android.multiple.ViewfinderView { <init>(...); }

